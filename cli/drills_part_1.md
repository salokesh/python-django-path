# Drills Part 1:

(You need to execute a bunch of git commands. Save all your commands in a text file called `cli_drill_part_1.txt`

1. Create the following directory structure. (Create empty files where necessary)

```
hello
├── five
│   └── six
│       ├── c.txt
│       └── seven
│           └── error.log
└── one
    ├── a.txt
    ├── b.txt
    └── two
        ├── d.txt
        └── three
            ├── e.txt
            └── four
                └── access.log
```

2. Delete all the files with the .log extension

3. Add the following content to a.txt

```
Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.
```

4. Delete the directory named `five`.

## Learning Outcomes

You must be able to do the following operations using the command line

1. Create a directory
2. Create a file within a particular directory
3. Navigate through various directories
4. List all the files in a directory
5. Copy a file / directory
6. Move a file / directory
5. Rename a file
6. Delete a file
7. Edit contents of a file
