![decorator](http://www.xappsoftware.com/wordpress/wp-content/uploads/2017/12/decorators.png
)

_______

## Resources:

* https://realpython.com/primer-on-python-decorators/
* https://hackernoon.com/decorators-in-python-8fd0dce93c08

______
## Drills:

__________

### Function Timer - One argument

Let's say you have two functions, `square` and `sqrt`. 

```
import math

def sqrt(x):
    return math.sqrt(x)

def square(x):
    return x * x
```

You want to know which of the above is faster. `square`, or `sqrt`. Decorate both the functions with a single decorator, called `timer`, that prints how much time is taken to evaluate the above functions.

__________

### Generic Function Timer

Now, you are keen to find how much time it takes to evaluate any function - that takes any number of arguments - by just decorating it. Define a decorator function that prints how much time is taken to evaluate any function.


Add the `generic_timer` decorator to the following functions:

```
def add(x, y):
    return x + y

def add3(x, y, z):
    return x + y + z

def add_any(*args):
    return sum(args)

def abs_add_any(*args, **kwargs):
    total = sum(args)
    if kwargs.get('abs') is True:
        return abs(total)
    return total
```

___________

### Fibonacci - cache


```
def fib(n):
    print("Computing fib({})".format(n))
    if n in [0, 1]:
        return n
    return fib(n - 1) + fib(n - 2)
```

Compute `fib(10)`. You can see that the same computations are repeated multiple times. How to speed it up?

Memoization - https://en.wikipedia.org/wiki/Memoization

Define a `memoize` decorator, that caches the result of any function which takes one argument. Decorate `fib(n)` with `memoize`, and notice that each `fib(n)` is computed only for each `n`.

____________

### lru_cache

Use the `lru_cache` decorator from the `functools` module to memoize the below function:

```
def factorial(n):
    print("Computing factorial({})".format(n))
    fact = 1
    for i in range(1, n + 1):
        fact *= i
    return fact
```

Experiment with different values. 

__________